package com.shollmann.android.melipayment.model;

import com.google.gson.annotations.SerializedName;

public class PaymentMethod {
    private String id;
    private String name;
    @SerializedName("payment_type_id")
    private String paymentTypeId;
    private String status;
    private String thumbnail;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPaymentTypeId() {
        return paymentTypeId;
    }

    public String getStatus() {
        return status;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public boolean isActive() {
        return "active".equals(status);
    }
}