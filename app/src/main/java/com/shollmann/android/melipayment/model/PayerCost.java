package com.shollmann.android.melipayment.model;

import com.google.gson.annotations.SerializedName;

public class PayerCost {
    private int installments;
    @SerializedName("installmentRate")
    private double installmentRate;
    @SerializedName("recommended_message")
    private String recommendedMessage;
    @SerializedName("installment_amount")
    private double installmentAmount;
    @SerializedName("total_amount")
    private double totalAmount;

    public int getInstallments() {
        return installments;
    }

    public double getInstallmentRate() {
        return installmentRate;
    }

    public String getRecommendedMessage() {
        return recommendedMessage;
    }

    public double getInstallmentAmount() {
        return installmentAmount;
    }

    public double getTotalAmount() {
        return totalAmount;
    }
}

