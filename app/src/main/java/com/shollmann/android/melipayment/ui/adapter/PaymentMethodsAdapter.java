package com.shollmann.android.melipayment.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.PaymentMethod;
import com.shollmann.android.melipayment.ui.viewholder.PaymentMethodViewHolder;

import java.util.List;

public class PaymentMethodsAdapter extends RecyclerView.Adapter<PaymentMethodViewHolder> {
    private List<PaymentMethod> paymentMethodList;

    @Override
    public PaymentMethodViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_viewholder, parent, false);
        return new PaymentMethodViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PaymentMethodViewHolder holder, int position) {
        holder.bindData(paymentMethodList.get(position));
    }

    @Override
    public int getItemCount() {
        return paymentMethodList.size();
    }

    public void setPaymentMethods(List<PaymentMethod> paymentMethodList) {
        this.paymentMethodList = paymentMethodList;
    }
}
