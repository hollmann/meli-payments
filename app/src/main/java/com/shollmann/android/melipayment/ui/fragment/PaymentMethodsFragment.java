package com.shollmann.android.melipayment.ui.fragment;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shollmann.android.melipayment.MeliPaymentApplication;
import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.api.CallId;
import com.shollmann.android.melipayment.api.CallOrigin;
import com.shollmann.android.melipayment.api.CallType;
import com.shollmann.android.melipayment.api.PaymentMethodApi;
import com.shollmann.android.melipayment.databinding.FragmentListBinding;
import com.shollmann.android.melipayment.model.PaymentMethod;
import com.shollmann.android.melipayment.ui.BaseFragment;
import com.shollmann.android.melipayment.ui.activity.PaymentActivity;
import com.shollmann.android.melipayment.ui.adapter.PaymentMethodsAdapter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentMethodsFragment extends BaseFragment<FragmentListBinding> implements Callback {
    private CallId mGetPaymentMethodCallId;
    private PaymentMethodApi mApi;
    private FragmentListBinding mBinding;
    private PaymentMethodsAdapter mAdapter;

    public static PaymentMethodsFragment newInstance() {
        return new PaymentMethodsFragment();
    }

    @NonNull
    @Override
    protected FragmentListBinding createBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        FragmentListBinding binding = FragmentListBinding.inflate(inflater, container, false);
        binding.setList(null);
        mBinding = binding;

        mApi = MeliPaymentApplication.getApplication().getApiPaymentMethods();

        initActionBar(true, getString(R.string.select_payment_method));

        return binding;
    }

    private void initRecyclerView() {
        mBinding.recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerview.setHasFixedSize(true);

        mAdapter = new PaymentMethodsAdapter();
        mBinding.recyclerview.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        initRecyclerView();
        getPaymentMethodsAsync();
    }

    private void getPaymentMethodsAsync() {
        mGetPaymentMethodCallId = new CallId(CallOrigin.STEP_PAYMENT_METHODS, CallType.GET_PAYMENT_METHODS);
        mApi.registerCallback(mGetPaymentMethodCallId, this);
        mApi.getPaymentMethods(mGetPaymentMethodCallId, this);
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (getBinding() == null) {
            return;
        }
        List<PaymentMethod> paymentMethodList = (List<PaymentMethod>) response.body();
        getBinding().setList(paymentMethodList);
        mAdapter.setPaymentMethods(getOnlyCards(paymentMethodList));
    }


    @Override
    public void onFailure(Call call, Throwable t) {
        mBinding.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), R.string.error_retrieving_data, Toast.LENGTH_LONG).show();
    }

    private List<PaymentMethod> getOnlyCards(List<PaymentMethod> paymentMethodList) {
        List<PaymentMethod> cardsList = new ArrayList<>();
        for (PaymentMethod method : paymentMethodList) {
            if ("credit_card".equals(method.getPaymentTypeId()) && method.isActive()) {
                cardsList.add(method);
            }
        }
        return cardsList;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PaymentMethod paymentMethod) {
        PaymentActivity activity = (PaymentActivity) getActivity();
        activity.setPaymentMethod(paymentMethod);
        activity.open(CardIssuersFragment.newInstance());
    }

    @Override
    public void onStop() {
        super.onStop();
        mApi.unregisterCallback(mGetPaymentMethodCallId);
    }
}
