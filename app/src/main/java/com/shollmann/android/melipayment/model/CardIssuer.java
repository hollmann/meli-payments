package com.shollmann.android.melipayment.model;

import com.google.gson.annotations.SerializedName;

public class CardIssuer {
    private String id;
    private String name;
    @SerializedName("secure_thumbnail")
    private String secureThumbnail;
    private String thumbnail;
    @SerializedName("processing_mode")
    private String processingMode;
    @SerializedName("merchant_account_id")
    private String merchantAccountId;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSecureThumbnail() {
        return secureThumbnail;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getProcessingMode() {
        return processingMode;
    }

    public String getMerchantAccountId() {
        return merchantAccountId;
    }
}
