package com.shollmann.android.melipayment.api;

import com.google.gson.reflect.TypeToken;
import com.shollmann.android.melipayment.api.contract.PaymentMethodsApiContract;
import com.shollmann.android.melipayment.model.CardIssuer;
import com.shollmann.android.melipayment.model.Installment;
import com.shollmann.android.melipayment.model.PaymentMethod;

import java.util.List;

import retrofit2.Callback;

public class PaymentMethodApi extends Api<PaymentMethodsApiContract> {

    PaymentMethodApi(Builder builder) {
        super(builder);
    }

    public void getPaymentMethods(CallId callId, Callback<List<PaymentMethod>> callback) {
        Cache cache = new Cache.Builder()
                .policy(Cache.Policy.CACHE_ELSE_NETWORK)
                .ttl(Cache.Time.FIVE_MINUTES)
                .key("get_payment_methods")
                .build();

        BaseApiCall<List<PaymentMethod>> apiCall = registerCall(callId, cache, callback, new TypeToken<List<PaymentMethod>>() {
        }.getType());

        if (apiCall != null && apiCall.requiresNetworkCall()) {
            getService().getPaymentMethods().enqueue(apiCall);
        }
    }

    public void getCardIssuers(CallId callId, String paymentMethodId, Callback<List<CardIssuer>> callback) {
        Cache cache = new Cache.Builder()
                .policy(Cache.Policy.CACHE_ELSE_NETWORK)
                .ttl(Cache.Time.FIVE_MINUTES)
                .key(String.format("get_card_issuers_%1$s", paymentMethodId))
                .build();

        BaseApiCall<List<CardIssuer>> apiCall = registerCall(callId, cache, callback, new TypeToken<List<CardIssuer>>() {
        }.getType());

        if (apiCall != null && apiCall.requiresNetworkCall()) {
            getService().getCardIssuers(paymentMethodId).enqueue(apiCall);
        }
    }


    public void getInstallments(CallId callId, double amount, String paymentMethodId, String issuerId, Callback<List<Installment>> callback) {
        Cache cache = new Cache.Builder()
                .policy(Cache.Policy.CACHE_ELSE_NETWORK)
                .ttl(Cache.Time.FIVE_MINUTES)
                .key(String.format("get_installments_%1$s_%2$s_%3$s", amount, paymentMethodId, issuerId))
                .build();

        BaseApiCall<List<Installment>> apiCall = registerCall(callId, cache, callback, new TypeToken<List<Installment>>() {
        }.getType());

        if (apiCall != null && apiCall.requiresNetworkCall()) {
            getService().getInstallments(amount, paymentMethodId, issuerId).enqueue(apiCall);
        }
    }

    public static class Builder extends Api.Builder {
        @Override
        public PaymentMethodApi build() {
            super.validate();
            return new PaymentMethodApi(this);
        }
    }

}
