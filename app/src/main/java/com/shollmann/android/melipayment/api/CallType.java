package com.shollmann.android.melipayment.api;

public enum CallType {
    GET_PAYMENT_METHODS,
    GET_ISSUERS,
    GET_INSTALLMENTS
}