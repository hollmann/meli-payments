package com.shollmann.android.melipayment.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.util.NavigationManager;

public class BaseActivity extends AppCompatActivity {

    protected NavigationManager mNavigationManager;
    private Toolbar mToolbar;
    private FrameLayout mContentLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mNavigationManager = new NavigationManager();
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(R.layout.activity_toolbar);
        mToolbar = findViewById(R.id.toolbar);
        mContentLayout = findViewById(R.id.content);

        setSupportActionBar(mToolbar);

        getLayoutInflater().inflate(layoutResID, mContentLayout);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mNavigationManager.navigateBack(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                mNavigationManager.navigateBack(this);
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    public void open(BaseFragment fragment) {
        mNavigationManager.open(fragment);
    }

    public void openAsRoot(BaseFragment fragment) {
        mNavigationManager.openAsRoot(fragment);
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
