package com.shollmann.android.melipayment.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.CardIssuer;
import com.shollmann.android.melipayment.ui.viewholder.CardIssuerViewHolder;

import java.util.List;

public class CardIssuersAdapter extends RecyclerView.Adapter<CardIssuerViewHolder> {
    private List<CardIssuer> issuersList;

    @Override
    public CardIssuerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_viewholder, parent, false);
        return new CardIssuerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CardIssuerViewHolder holder, int position) {
        holder.bindData(issuersList.get(position));
    }

    @Override
    public int getItemCount() {
        return issuersList.size();
    }

    public void setIssuers(List<CardIssuer> issuersList) {
        this.issuersList = issuersList;
    }
}
