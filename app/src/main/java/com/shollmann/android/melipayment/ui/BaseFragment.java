package com.shollmann.android.melipayment.ui;

import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shollmann.android.melipayment.R;

import org.greenrobot.eventbus.EventBus;


public abstract class BaseFragment<B extends ViewDataBinding> extends Fragment {

    private B mBinding;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mBinding = createBinding(inflater, container);
        return mBinding.getRoot();
    }

    @NonNull
    protected abstract B createBinding(LayoutInflater inflater, @Nullable ViewGroup container);


    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    protected void initActionBar(boolean showHomeButton, String title) {
        if (getActivity() != null && getActivity() instanceof BaseActivity) {
            ActionBar supportActionBar = ((BaseActivity) getActivity()).getSupportActionBar();
            supportActionBar.setHomeButtonEnabled(showHomeButton);
            supportActionBar.setDisplayHomeAsUpEnabled(showHomeButton);
            supportActionBar.setTitle(title);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mBinding != null) {
            mBinding.unbind();
            mBinding = null;
        }
    }

    @Nullable
    public B getBinding() {
        return mBinding;
    }

    @Override
    public void onStart() {
        super.onStart();
        try {
            EventBus.getDefault().register(this);
        } catch (Throwable t) {
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        try {
            EventBus.getDefault().unregister(this);
        } catch (Throwable t) {
        }
    }
}