package com.shollmann.android.melipayment.api;

public enum CallOrigin {
    STEP_PAYMENT_METHODS,
    STEP_ISSUERS,
    STEP_INSTALLMENTS,
}
