package com.shollmann.android.melipayment.api.contract;


import com.shollmann.android.melipayment.model.CardIssuer;
import com.shollmann.android.melipayment.model.Installment;
import com.shollmann.android.melipayment.model.PaymentMethod;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface PaymentMethodsApiContract {
    @GET("/v1/payment_methods")
    Call<List<PaymentMethod>> getPaymentMethods();

    @GET("/v1/payment_methods/card_issuers")
    Call<List<CardIssuer>> getCardIssuers(
            @Query("payment_method_id") String paymentMethodId);

    @GET("/v1/payment_methods/installments")
    Call<List<Installment>> getInstallments(
            @Query("amount") double amount,
            @Query("payment_method_id") String paymentMethodId,
            @Query("issuer.id") String issuerId);

}



