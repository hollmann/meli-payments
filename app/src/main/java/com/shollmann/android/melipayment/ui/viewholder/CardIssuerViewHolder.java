package com.shollmann.android.melipayment.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.CardIssuer;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

public class CardIssuerViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTxtName;
    private final ImageView mImgThumbnail;

    public CardIssuerViewHolder(View itemView) {
        super(itemView);
        mTxtName = itemView.findViewById(R.id.text);
        mImgThumbnail = itemView.findViewById(R.id.thumbnail);
    }

    public void bindData(final CardIssuer cardIssuer) {
        mTxtName.setText(cardIssuer.getName());
        Picasso.with(mImgThumbnail.getContext())
                .load(cardIssuer.getThumbnail())
                .into(mImgThumbnail);
        itemView.setOnClickListener(v -> EventBus.getDefault().post(cardIssuer));
    }
}
