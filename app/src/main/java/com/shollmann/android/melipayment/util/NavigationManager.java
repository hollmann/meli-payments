package com.shollmann.android.melipayment.util;


import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.ui.fragment.AmountFragment;
import com.shollmann.android.melipayment.ui.fragment.PaymentMethodsFragment;


public class NavigationManager {

    private FragmentManager mFragmentManager;
    private NavigationListener mNavigationListener;

    public void init(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
        mFragmentManager.addOnBackStackChangedListener(() -> {
            if (mNavigationListener != null) {
                mNavigationListener.onBackstackChanged();
            }
        });
    }

    public void open(Fragment fragment) {
        if (mFragmentManager != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.main_container, fragment)
                    .setCustomAnimations(R.anim.slide_in_left,
                            R.anim.slide_out_right,
                            R.anim.slide_in_right,
                            R.anim.slide_out_left)
                    .addToBackStack(fragment.toString())
                    .commit();
        }
    }

    public void openAsRoot(Fragment fragment) {
        popEveryFragment();
        open(fragment);
    }

    private void popEveryFragment() {
        int backStackCount = mFragmentManager.getBackStackEntryCount();

        for (int i = 0; i < backStackCount; i++) {
            int backStackId = mFragmentManager.getBackStackEntryAt(i).getId();
            mFragmentManager.popBackStack(backStackId, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public void navigateBack(Activity baseActivity) {
        if (mFragmentManager.getBackStackEntryCount() == 0) {
            baseActivity.finish();
        } else {
            mFragmentManager.popBackStackImmediate();
        }
    }

    public void startEnterAmount() {
        Fragment fragment = AmountFragment.newInstance();
        openAsRoot(fragment);
    }

    public boolean isRootFragmentVisible() {
        return mFragmentManager.getBackStackEntryCount() <= 1;
    }

    public NavigationListener getNavigationListener() {
        return mNavigationListener;
    }

    public void setNavigationListener(NavigationListener navigationListener) {
        mNavigationListener = navigationListener;
    }

    public interface NavigationListener {
        void onBackstackChanged();
    }

}
