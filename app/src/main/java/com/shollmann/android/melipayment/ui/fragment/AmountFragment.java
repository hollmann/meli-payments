package com.shollmann.android.melipayment.ui.fragment;


import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.databinding.FragmentAmountBinding;
import com.shollmann.android.melipayment.model.PaymentConfig;
import com.shollmann.android.melipayment.ui.BaseFragment;
import com.shollmann.android.melipayment.ui.activity.PaymentActivity;

public class AmountFragment extends BaseFragment<FragmentAmountBinding> {
    private static boolean mIsFinishMode;
    private FragmentAmountBinding mBinding;

    public static AmountFragment newInstance() {
        return new AmountFragment();
    }

    public static AmountFragment newInstance(boolean isFinishMode) {
        mIsFinishMode = isFinishMode;
        return new AmountFragment();
    }

    @NonNull
    @Override
    protected FragmentAmountBinding createBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        FragmentAmountBinding binding = FragmentAmountBinding.inflate(inflater, container, false);
        mBinding = binding;
        binding.setFragment(this);

        initActionBar(false, getString(R.string.app_name));

        if (mIsFinishMode) {
            showPaymentConfigDialog();
        }
        return binding;
    }

    private void showPaymentConfigDialog() {
        PaymentConfig config = ((PaymentActivity) getActivity()).getPaymentConfig();
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.payment_config_title)
                .setMessage(String.format(getString(R.string.payment_config_body),
                        String.format("%.2f",config.getAmount()),
                        config.getPaymentMethod().getName(),
                        config.getCardIssuer() != null ? config.getCardIssuer().getName() : getString(R.string.no_card_issuer),
                        config.getPayerCost() != null ? config.getPayerCost().getRecommendedMessage()
                                : getString(R.string.no_installments)))
                .setOnDismissListener(dialog -> ((PaymentActivity)getActivity()).resetPaymentConfig())
                .show();
    }

    public void goToSelectPaymentMethod() {
        String amountString = mBinding.edtAmount.getText().toString();
        if (!TextUtils.isEmpty(amountString)) {
            PaymentActivity activity = (PaymentActivity) getActivity();
            Float amount = Float.valueOf(amountString);
            activity.hideKeyboard(mBinding.edtAmount);
            activity.setAmount(amount);
            activity.open(PaymentMethodsFragment.newInstance());
        } else {
            mBinding.edtAmount.setError(getString(R.string.enter_valid_value));
        }
    }

    @Override
    public void onPause() {
        mIsFinishMode = false;
        super.onPause();
    }
}
