package com.shollmann.android.melipayment;

import android.app.Application;

import com.shollmann.android.melipayment.api.PaymentMethodApi;
import com.shollmann.android.melipayment.api.contract.PaymentMethodsApiContract;
import com.shollmann.android.melipayment.api.db.CacheDbHelper;
import com.shollmann.android.melipayment.util.Constants;

import org.greenrobot.eventbus.EventBus;

public class MeliPaymentApplication extends Application {
    private static MeliPaymentApplication instance;
    private CacheDbHelper cacheDbHelper;
    private PaymentMethodApi apiPaymentMethod;

    public static MeliPaymentApplication getApplication() {
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        this.instance = this;
        this.apiPaymentMethod = (PaymentMethodApi) new PaymentMethodApi.Builder()
                .baseUrl(Constants.API_BASE_URL)
                .contract(PaymentMethodsApiContract.class)
                .build();
        this.cacheDbHelper = new CacheDbHelper(getApplicationContext());
        EventBus.builder().installDefaultEventBus();
    }

    public CacheDbHelper getCacheDbHelper() {
        return cacheDbHelper;
    }

    public PaymentMethodApi getApiPaymentMethods() {
        return apiPaymentMethod;
    }
}
