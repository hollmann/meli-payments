package com.shollmann.android.melipayment.ui.fragment;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shollmann.android.melipayment.MeliPaymentApplication;
import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.api.CallId;
import com.shollmann.android.melipayment.api.CallOrigin;
import com.shollmann.android.melipayment.api.CallType;
import com.shollmann.android.melipayment.api.PaymentMethodApi;
import com.shollmann.android.melipayment.databinding.FragmentListBinding;
import com.shollmann.android.melipayment.model.CardIssuer;
import com.shollmann.android.melipayment.ui.BaseActivity;
import com.shollmann.android.melipayment.ui.BaseFragment;
import com.shollmann.android.melipayment.ui.activity.PaymentActivity;
import com.shollmann.android.melipayment.ui.adapter.CardIssuersAdapter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CardIssuersFragment extends BaseFragment<FragmentListBinding> implements Callback {
    private CallId mGetCardIssuersCallId;
    private PaymentMethodApi mApi;
    private FragmentListBinding mBinding;
    private CardIssuersAdapter mAdapter;

    public static CardIssuersFragment newInstance() {
        return new CardIssuersFragment();
    }

    @NonNull
    @Override
    protected FragmentListBinding createBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        FragmentListBinding binding = FragmentListBinding.inflate(inflater, container, false);
        binding.setList(null);
        mBinding = binding;

        initActionBar(true, getString(R.string.select_card_issuer));

        mApi = MeliPaymentApplication.getApplication().getApiPaymentMethods();

        return binding;
    }

    private void initRecyclerView() {
        mBinding.recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerview.setHasFixedSize(true);

        mAdapter = new CardIssuersAdapter();
        mBinding.recyclerview.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        initRecyclerView();
        getCardIssuersAsync();
    }

    private void getCardIssuersAsync() {
        mGetCardIssuersCallId = new CallId(CallOrigin.STEP_ISSUERS, CallType.GET_ISSUERS);
        mApi.registerCallback(mGetCardIssuersCallId, this);
        mApi.getCardIssuers(mGetCardIssuersCallId, ((PaymentActivity) getActivity()).getPaymentConfig().getPaymentMethod().getId(), this);
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (getBinding() == null) {
            return;
        }
        List<CardIssuer> issuersList = (List<CardIssuer>) response.body();
        if (issuersList != null && !issuersList.isEmpty()) {
            getBinding().setList(issuersList);
            mAdapter.setIssuers(issuersList);
        } else {
            ((BaseActivity)getActivity()).openAsRoot(AmountFragment.newInstance(true));
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        mBinding.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), R.string.error_retrieving_data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        mApi.unregisterCallback(mGetCardIssuersCallId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(CardIssuer cardIssuer) {
        PaymentActivity activity = (PaymentActivity) getActivity();
        activity.setCardIssuer(cardIssuer);
        activity.open(InstallmentsFragment.newInstance());
    }
}
