package com.shollmann.android.melipayment.ui.activity;

import android.os.Bundle;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.CardIssuer;
import com.shollmann.android.melipayment.model.PayerCost;
import com.shollmann.android.melipayment.model.PaymentConfig;
import com.shollmann.android.melipayment.model.PaymentMethod;
import com.shollmann.android.melipayment.ui.BaseActivity;

public class PaymentActivity extends BaseActivity {
    private PaymentConfig mPaymentConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        mNavigationManager.init(getSupportFragmentManager());
        mNavigationManager.startEnterAmount();

        mPaymentConfig = new PaymentConfig();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            mNavigationManager.navigateBack(this);
        }
    }

    public void setPaymentMethod(PaymentMethod method) {
        mPaymentConfig.setPaymentMethod(method);
    }

    public PaymentConfig getPaymentConfig() {
        return mPaymentConfig;
    }

    public void setCardIssuer(CardIssuer cardIssuer) {
        mPaymentConfig.setCardIssuer(cardIssuer);
    }

    public void setPayerCost(PayerCost payerCost) {
        mPaymentConfig.setPayerCost(payerCost);
    }

    public void setAmount(Float amount) {
        mPaymentConfig.setAmount(amount);
    }

    public void resetPaymentConfig() {
        mPaymentConfig = new PaymentConfig();
    }
}