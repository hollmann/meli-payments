package com.shollmann.android.melipayment.ui.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.PayerCost;

import org.greenrobot.eventbus.EventBus;

public class PayerCostViewHolder extends RecyclerView.ViewHolder {

    private final TextView mTxtName;
    private final ImageView mImgThumbnail;

    public PayerCostViewHolder(View itemView) {
        super(itemView);
        mTxtName = itemView.findViewById(R.id.text);
        mImgThumbnail = itemView.findViewById(R.id.thumbnail);
    }

    public void bindData(final PayerCost payerCost) {
        mTxtName.setText(payerCost.getRecommendedMessage());
        mImgThumbnail.setVisibility(View.GONE);
        itemView.setOnClickListener(v -> EventBus.getDefault().post(payerCost));
    }
}
