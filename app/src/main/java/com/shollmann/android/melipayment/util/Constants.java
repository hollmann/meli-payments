package com.shollmann.android.melipayment.util;

public class Constants {
    public static final String CACHE = "_melipayment_cache";
    public static final String PUBLIC_KEY = "444a9ef5-8a6b-429f-abdf-587639155d88";
    public static final String API_BASE_URL = "https://api.mercadopago.com";
}
