package com.shollmann.android.melipayment.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.model.Installment;
import com.shollmann.android.melipayment.model.PayerCost;
import com.shollmann.android.melipayment.ui.viewholder.CardIssuerViewHolder;
import com.shollmann.android.melipayment.ui.viewholder.PayerCostViewHolder;

import java.util.List;

public class PayerCostAdapter extends RecyclerView.Adapter<PayerCostViewHolder> {
    private List<PayerCost> payerCosts;

    @Override
    public PayerCostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_viewholder, parent, false);
        return new PayerCostViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PayerCostViewHolder holder, int position) {
        holder.bindData(payerCosts.get(position));
    }

    @Override
    public int getItemCount() {
        return payerCosts.size();
    }

    public void setPayerCosts(List<PayerCost> payerCosts) {
        this.payerCosts = payerCosts;
    }
}
