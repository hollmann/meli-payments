package com.shollmann.android.melipayment.ui.fragment;


import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.shollmann.android.melipayment.MeliPaymentApplication;
import com.shollmann.android.melipayment.R;
import com.shollmann.android.melipayment.api.CallId;
import com.shollmann.android.melipayment.api.CallOrigin;
import com.shollmann.android.melipayment.api.CallType;
import com.shollmann.android.melipayment.api.PaymentMethodApi;
import com.shollmann.android.melipayment.databinding.FragmentListBinding;
import com.shollmann.android.melipayment.model.Installment;
import com.shollmann.android.melipayment.model.PayerCost;
import com.shollmann.android.melipayment.model.PaymentConfig;
import com.shollmann.android.melipayment.ui.BaseActivity;
import com.shollmann.android.melipayment.ui.BaseFragment;
import com.shollmann.android.melipayment.ui.activity.PaymentActivity;
import com.shollmann.android.melipayment.ui.adapter.PayerCostAdapter;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InstallmentsFragment extends BaseFragment<FragmentListBinding> implements Callback {
    private CallId mGetInstallmentsCallId;
    private PaymentMethodApi mApi;
    private FragmentListBinding mBinding;
    private PayerCostAdapter mAdapter;

    public static InstallmentsFragment newInstance() {
        return new InstallmentsFragment();
    }

    @NonNull
    @Override
    protected FragmentListBinding createBinding(LayoutInflater inflater, @Nullable ViewGroup container) {
        FragmentListBinding binding = FragmentListBinding.inflate(inflater, container, false);
        binding.setList(null);
        mBinding = binding;

        initActionBar(true, getString(R.string.select_installment));

        mApi = MeliPaymentApplication.getApplication().getApiPaymentMethods();

        return binding;
    }

    private void initRecyclerView() {
        mBinding.recyclerview.setLayoutManager(new LinearLayoutManager(getContext()));
        mBinding.recyclerview.setHasFixedSize(true);

        mAdapter = new PayerCostAdapter();
        mBinding.recyclerview.setAdapter(mAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        initRecyclerView();
        getInstallmentsAsync();
    }

    private void getInstallmentsAsync() {
        mGetInstallmentsCallId = new CallId(CallOrigin.STEP_ISSUERS, CallType.GET_ISSUERS);
        mApi.registerCallback(mGetInstallmentsCallId, this);
        PaymentConfig paymentConfig = ((PaymentActivity) getActivity()).getPaymentConfig();
        mApi.getInstallments(mGetInstallmentsCallId, paymentConfig.getAmount(), paymentConfig.getPaymentMethod().getId(), paymentConfig.getCardIssuer().getId(), this);
    }

    @Override
    public void onResponse(Call call, Response response) {
        if (getBinding() == null) {
            return;
        }
        List<Installment> installments = (List<Installment>) response.body();
        getBinding().setList(installments);
        if (installments != null) {
            List<PayerCost> payerCosts = installments.get(0).getPayerCosts();
            if (payerCosts != null && !payerCosts.isEmpty()) {
                mAdapter.setPayerCosts(payerCosts);
            } else {
                ((BaseActivity) getActivity()).openAsRoot(AmountFragment.newInstance(true));
            }
        } else {
            Toast.makeText(getContext(), R.string.error_retrieving_data, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFailure(Call call, Throwable t) {
        mBinding.progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), R.string.error_retrieving_data, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onStop() {
        super.onStop();
        mApi.unregisterCallback(mGetInstallmentsCallId);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(PayerCost payerCost) {
        PaymentActivity activity = (PaymentActivity) getActivity();
        activity.setPayerCost(payerCost);
        activity.openAsRoot(AmountFragment.newInstance(true));
    }
}
